const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

//JSON Web Token or JWT is a way of securely passing information from the server to the front end or to other parts of server
//Information is kept secure through the use of secret code


module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	//Generate a JSON web token using the jwt's method (sign())

	return jwt.sign(data, secret, {})
}